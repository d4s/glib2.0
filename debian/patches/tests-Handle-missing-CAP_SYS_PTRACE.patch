From: Sjoerd Simons <sjoerd.simons@collabora.co.uk>
Date: Tue, 28 Aug 2018 16:55:14 +0200
Subject: tests: Handle missing CAP_SYS_PTRACE

Even if gdb is installed if CAP_SYS_PTRACE is missing (e.g. due to running
in a container) the test will still fail. Unfortunately gdb doesn't
really seem to have a way of telling that a command in the batch
commands failed.

Work around this by printing a marker after the program has run and if
the program run failed for some reason skip the test.

Signed-off-by: Sjoerd Simons <sjoerd.simons@collabora.co.uk>
---
 tests/assert-msg-test.gdb    |  1 +
 tests/run-assert-msg-test.sh | 11 ++++++++++-
 2 files changed, 11 insertions(+), 1 deletion(-)

diff --git a/tests/assert-msg-test.gdb b/tests/assert-msg-test.gdb
index 63a2541..a30ccd9 100644
--- a/tests/assert-msg-test.gdb
+++ b/tests/assert-msg-test.gdb
@@ -1,4 +1,5 @@
 run
+print "program ran"
 set print elements 0
 print (char*) __glib_assert_msg
 quit
diff --git a/tests/run-assert-msg-test.sh b/tests/run-assert-msg-test.sh
index d4a5111..d2f718f 100755
--- a/tests/run-assert-msg-test.sh
+++ b/tests/run-assert-msg-test.sh
@@ -40,8 +40,17 @@ fi
 echo_v "Running gdb on assert-msg-test"
 OUT=$($LIBTOOL --mode=execute gdb --batch -x ${srcdir:-.}/assert-msg-test.gdb ./assert-msg-test 2> $error_out) || fail "failed to run gdb"
 
+# When building without CAP_SYS_PTRACE enabled gdb might be able to run but not
+# actually examing the program. Detect that by having a looking for the marker
+# that's after the run command
+echo_v "Checking if gdb could run program"
+if ! echo "$OUT" | grep -q '^$1.*program ran' ; then
+  echo_v "Skipped (gdb couldn't run program)"
+  exit 0
+fi
+
 echo_v "Checking if assert message is in __glib_assert_msg"
-if ! echo "$OUT" | grep -q '^$1.*"GLib:ERROR:.*assert-msg-test.c:.*:.*main.*: assertion failed: (42 < 0)"'; then
+if ! echo "$OUT" | grep -q '^$2.*"GLib:ERROR:.*assert-msg-test.c:.*:.*main.*: assertion failed: (42 < 0)"'; then
   fail "__glib_assert_msg does not have assertion message"
 fi
 
